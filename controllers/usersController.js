
const validator = require('validator')
const bcrypt = require('bcrypt')

const PER_PAGE = 20


index =( (req, res) => {
    const dbPool = req.dbPool;

    // Use the connection pool to execute SQL queries
    dbPool.getConnection((err, connection) => {   
      if (err) {
        console.error('Error getting a MySQL connection:', err);
        res.status(500).send('Internal Server Error');
        return;
      }
  
      // Execute your SQL query using the 'connection' object
      const page = req.query.page || 1;
      const limit =  PER_PAGE * (page -1)
      let total = 0
      connection.query("SELECT count(*) as count FROM users", (error, results) => {
    
          if (error) {
            console.error('Error executing SQL query:', error);
            res.status(500).send('Internal Server Error');
            return;
          }
    
          // Process the query results and send a response
          total = results[0].count;
          connection.release();
        });
  
      connection.query(`SELECT * FROM users LIMIT  ${limit}, 20`, (error, results) => {
  
        if (error) {
          console.error('Error executing SQL query:', error);
          res.status(500).send('Internal Server Error');
          return;
        }
        console.log(req.sessionID)
        // Process the query results and send a response
        res.json({
                    "total" : total,
                    "current_page" : page,
                    "pages" : Math.ceil(total / 20),
                    data : results  
        });
      });
    });
});

user_show = ((req, res) => {
  const dbPool = req.dbPool;
  const user_id = req.params.id
  dbPool.getConnection((err, connection) => {   
      if (err) {
        console.error('Error getting a MySQL connection:', err);
        res.status(500).send('Internal Server Error');
        return;
      }
  
  connection.query(`SELECT * FROM users WHERE id=${user_id}`, (error, results) => {
      if (error) {
          console.error('Error executing SQL query:', error);
          res.status(500).send('Internal Server Error');
          return;
        }
      res.json(results)  
      });
  });
});

user_delete = ( (req, res) => {
  const dbPool = req.dbPool;
  const user_id = req.params.id
  dbPool.getConnection((err, connection) => {   
      if (err) {
        console.error('Error getting a MySQL connection:', err);
        res.status(500).send('Internal Server Error');
        return;
      }
  
  connection.query(`DELETE  FROM users WHERE id=${user_id}`, (error, results) => {
      if (error) {
          console.error('Error executing SQL query:', error);
          res.status(500).send('Internal Server Error');
          return;
        }
      res.status(203).json(`User with id ${user_id} has been deleted`)  
      });
  });
});


user_create= ((req, res) => {
  const dbPool = req.dbPool;
  dbPool.getConnection(async (err, connection) => {   
      if (err) {
        console.error('Error getting a MySQL connection:', err);
        res.status(500).send('Internal Server Error');
        return;
      }
    const { email, password} = req.body
    let hashedPassword = ''

    if (!validator.isEmail(email)) {
      res.status(400).send("Invallid email address")
    }

    connection.query(`SELECT * FROM users WHERE email="${email}"`, (error, results) => {
      if (err) {
        res.status(400).send("Can't encrypt the password");
        return ;
      }
      console.log(results);

    });
     bcrypt.hash(password, 10, function(err, hash) {
      if (err) {
        res.status(400).send("Can't encrypt the password");
        return ;
      }
      console.log("passwords ; " ,password, hashedPassword)
      connection.query(`INSERT INTO users(email, password) VALUES ("${email}", "${hash}")`, (error, results) => {
          if (error) {
              if (error.errno = 1062) { //DUPLICATE ENTRY
                res.status(400).send('email already exists');
                return;
              }  else {
                res.status(500).send('Unknown error');
                return
              }
            }
          console.log(results)  
          res.status(201).send('User created')  
          });
    });
      connection.release();
  });
});

user_update = (( req, res) => {
  const dbPool = req.dbPool;
  dbPool.getConnection(async(err, connection) => {   
      if (err) {
        console.error('Error getting a MySQL connection:', err);
        res.status(500).send('Internal Server Error');
        return;
      }
  const user_id = req.params.id
  const {name,  email, active, status} = req.body
  await connection.query(`UPDATE users SET ", email= "${email}", active=${active} WHERE id=${user_id}`, (error, results) => {
      if (error) {
          console.error('Error executing SQL query:', error);
          res.status(500).send('Internal Server Error');
          return;
        }
      res.status(203).send(`User with id ${user_id} as been updated`)  
      });
      connection.release();
  });
})