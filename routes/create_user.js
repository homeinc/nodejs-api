const { faker } = require('@faker-js/faker');


  function createRandomUser() {
  return {
    email: faker.internet.email(),
    password: '$2y$10$Bu7CxsHdlzsZogloijbMw.pYBmZAGWXe8zzx0HuGGvJak/WpH8OWu' ,
  };
}

module.exports = faker.helpers.multiple(createRandomUser, {
  count: 200,
});
