const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt')
const validator = require('validator')
const session = require('express-session')

router.post('/login', (req, res) => {
    const dbPool = req.dbPool;
    dbPool.getConnection((err, connection) => {   
        if (err) {
          console.error('Error getting a MySQL connection:', err);
          res.status(500).send('Internal Server Error');
          return;
        }
        const { email, password} = req.body


        if (!validator.isEmail(email)) {
            res.status(400).send("Invalid email")
            return ;
        }
        connection.query(`SELECT * FROM users WHERE email="${email}"`, (err, results) => {
            if (err ) {
                res.status(403).send("Unknown Database error")
                return;
            }
            if (results.length ==0) {
                res.status(403).send("Invalid email or password")
                return;
            }

            const user = results.pop()

            bcrypt.compare(password, user.password , (error, result) => {
                if (error) {
                    res.status(500).send("Error comparing password")
                    return 
                }
            console.log(result)    
            if (result) { 
                req.session.user= {
                    email : user.email,
                    role : user.role,
                    token : user.token
                }
                console.log(req.sessionID)
                res.status(200).send("Confirmed");
            }
            else
                res.status(403).send("Invalid email or password")
        });
        });
    });
})

router.get('/logout', (req, res)=> {
    req.session.destroy();
    res.status(200).send("User logged out")
})

module.exports=router