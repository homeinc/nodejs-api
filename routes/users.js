const express = require('express');
const router = express.Router();
require('../controllers/usersController')


router.route('/').get(index).post(user_create)
router.route('/:id')
.get(user_show)
.post(user_create)
.delete(user_delete)
.patch(user_update)

module.exports = router;





