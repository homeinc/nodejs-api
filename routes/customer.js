const express = require('express');
const router = express.Router();

const PER_PAGE = 20
// Define a route that uses the MySQL connection pool
router.get('/', (req, res) => {
  const dbPool = req.dbPool;

  // Use the connection pool to execute SQL queries
  dbPool.getConnection((err, connection) => {   
    if (err) {
      console.error('Error getting a MySQL connection:', err);
      res.status(500).send('Internal Server Error');
      return;
    }

    // Execute your SQL query using the 'connection' object
    const page = req.query.page || 1;
    const limit =  PER_PAGE * (page -1)
    let total = 0
    connection.query("SELECT count(*) as count FROM customer", (error, results) => {
  
        if (error) {
          console.error('Error executing SQL query:', error);
          res.status(500).send('Internal Server Error');
          return;
        }
  
        // Process the query results and send a response
        total = results[0].count;
        connection.release();
      });

    connection.query(`SELECT * FROM customer LIMIT  ${limit}, 20`, (error, results) => {

      if (error) {
        console.error('Error executing SQL query:', error);
        res.status(500).send('Internal Server Error');
        return;
      }

      // Process the query results and send a response
      res.json({
                  "total" : total,
                  "pages" : Math.ceil(total / 20),
                  data : results  
      });
    });
  });
});

router.get('/:id', (req, res) => {
    const dbPool = req.dbPool;
    const user_id = req.params.id
    dbPool.getConnection((err, connection) => {   
        if (err) {
          console.error('Error getting a MySQL connection:', err);
          res.status(500).send('Internal Server Error');
          return;
        }
    
    connection.query(`SELECT * FROM customer WHERE id=${user_id}`, (error, results) => {
        if (error) {
            console.error('Error executing SQL query:', error);
            res.status(500).send('Internal Server Error');
            return;
          }
        res.json(results)  
        });
    });
})

router.delete('/:id', (req, res) => {
    const dbPool = req.dbPool;
    const user_id = req.params.id
    dbPool.getConnection((err, connection) => {   
        if (err) {
          console.error('Error getting a MySQL connection:', err);
          res.status(500).send('Internal Server Error');
          return;
        }
    
    connection.query(`DELETE  FROM customer WHERE id=${user_id}`, (error, results) => {
        if (error) {
            console.error('Error executing SQL query:', error);
            res.status(500).send('Internal Server Error');
            return;
          }
        res.status(203).json(`User with id ${user_id} has been deleted`)  
        });
    });
})

router.post('/', (req, res) => {
    const dbPool = req.dbPool;
    dbPool.getConnection((err, connection) => {   
        if (err) {
          console.error('Error getting a MySQL connection:', err);
          res.status(500).send('Internal Server Error');
          return;
        }
    
    // connection.query(`DELETE  FROM customer WHERE id=${user_id}`, (error, results) => {
    //     if (error) {
    //         console.error('Error executing SQL query:', error);
    //         res.status(500).send('Internal Server Error');
    //         return;
    //       }
    //     res.send(203, `User with id ${user_id} as been deleted`)  
    //     });
        console.log(req.body)
        connection.release();
    });
})

router.patch('/:id', async (req, res) => {
    const dbPool = req.dbPool;
    dbPool.getConnection(async(err, connection) => {   
        if (err) {
          console.error('Error getting a MySQL connection:', err);
          res.status(500).send('Internal Server Error');
          return;
        }
    const user_id = req.params.id
    const {first_name, last_name, email, active} = req.body
    await connection.query(`UPDATE customer SET first_name ="${first_name}", last_name="${last_name}", email= "${email}", active=${active} WHERE id=${user_id}`, (error, results) => {
        if (error) {
            console.error('Error executing SQL query:', error);
            res.status(500).send('Internal Server Error');
            return;
          }
        res.status(203).send(`User with id ${user_id} as been updated`)  
        });
        connection.release();
    });
})



module.exports = router;





