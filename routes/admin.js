const express = require('express');
const router = express.Router();
const session = require('express-session');
require('../middlewares/adminMiddleware');

router.get('/',  isAdmin, (req, res) => {
    res.status(200).send("Admin page")
    console.log(req.sessionID   )
    console.log(req.session.user)
})

module.exports=router