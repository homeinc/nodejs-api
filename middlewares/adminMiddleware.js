

isAdmin = ((req, res, next) => {
    console.log(req.session.user)
    if (req.session.user && req.session.user.role === 'ROLE_ADMIN') 
        return next();
    res.status(403).send('Forbidden')
})


module.exports = isAdmin