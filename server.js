const http = require('http')
const express = require('express')
const app = express()
const session = require('express-session');
const mysql = require('mysql')
require('dotenv').config()
const users = require('./routes/users')
const login = require('./routes/login')
const admin = require('./routes/admin')

// Initialize Passport and configure strategies


const dbPool = mysql.createPool({
  host:  process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME
});



app.use((req, res, next) => {
    req.dbPool = dbPool;
    next();
  });

  app.use(session({
    secret: 'my23487453*&^&superpuperKeYY', // Change this to a strong, unique secret
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false }, // Set 'secure' to 'true' for HTTPS
  })); 

app.use(express.json());
app.use(express.static('public'))

// Middleware for parsing form data
app.use(express.urlencoded({ extended: true }));

app.use('/users', users)
app.use(login)
app.use('/admin', admin)

const PORT = 3000;
app.post('/create_users', (req, res) => {
    const fake_users = require('./routes/create_user')
    const dbPool = req.dbPool;
    dbPool.getConnection((err, connection) => {   
        if (err) {
          console.error('Error getting a MySQL connection:', err);
          res.status(500).send('Internal Server Error');
          return;
        }
    fake_users.forEach(user => {
        connection.query(`INSERT INTO users(email, password) VALUES("${user.email}", "${user.password}")`, (error, results) => {
        if (error) {
            console.error('Error executing SQL query:', error);
            res.status(500).send('Internal Server Error');
            return;
          }
        });
        })
        connection.release();
    });
    res.status(201).send("Users created")  
});


app.listen(3000, ()=> {
    console.log(`Server is running on port ${PORT}`);
})